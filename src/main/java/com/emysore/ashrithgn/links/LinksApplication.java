package com.emysore.ashrithgn.links;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(scanBasePackages = {"com.emysore.ashrithgn"})
@EnableMongoRepositories
public class LinksApplication {

	public static void main(String[] args) {
		SpringApplication.run(LinksApplication.class, args);
	}
}
