package com.emysore.ashrithgn.links.controllers;

import com.emysore.ashrithgn.links.business.entity.UserEntity;
import com.emysore.ashrithgn.links.business.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping(path = "/api")
public class UserController {
    @Autowired
    UserRepository userRepository;

    @GetMapping("/alive")
    public String alive(){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        UserEntity user = new UserEntity();
        user.setUsername("ashrithgn");
        user.setEmail("ashrithgn@gmail.com");
        user.setCountryCode("+91");
        user.setPassword(encoder.encode("12345"));
        user.setCreatedAt(new Date());
        userRepository.save(user);
        return "alive";
    }

    @GetMapping("/user/1")
    public String user(){
        userRepository.findFirstByUsernameOrEmail("ashrith");
        return "alive";
    }
}
