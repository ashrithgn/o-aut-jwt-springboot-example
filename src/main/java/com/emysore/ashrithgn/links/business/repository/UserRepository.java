package com.emysore.ashrithgn.links.business.repository;

import com.emysore.ashrithgn.links.business.entity.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<UserEntity,String> {
    public UserEntity findFirstByUsernameOrEmail(String search);
}
